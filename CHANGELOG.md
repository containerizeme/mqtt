- [Mosquitto](#mosquitto)
- [Tinkerforge](#tinkerforge)

# Mosquitto
### 2.0.18-r2
* Update to Alpine 3.20.3
### 2.0.18-r1
* Update to Alpine 3.19.0
* mosquitto 2.0.18-r0
### 2.0.15-r2
* Upate to Alpien 3.17.3
### 2.0.15-r1
* Update Alpine 3.17.0 with Mosquitto 2.0.15-r1
### 2.0.14-r2
* Enabled MQTT broker on all interfaces (not only on loopback interface)
* Allow anonymous access by default
* Add `mosquitto.conf` and expose configuration volume (`/srv/mosquitto/`)
### 2.0.14-r0
* Update to Alpine 3.15.0 with Mosquitto 2.0.14-r0
### 2.0.11-r1
* Alpine v3.14.2 with Mosquitto v2.0.11-r0
### 1.6.12-r1
* Alpine v3.13.2 with Mosquitto v1.6.12
### 1.6.8-r1
* Alpine v3.11.3 with Mosquitto v1.6.8
### 1.6.3-r3
* Alpine v3.10.1 with Mosquitto v1.6.3

# Tinkerforge
### 2.0.17-r1
* Update Tinkerforge MQTT 2.0.17
* Update to Alpine 3.20.3
* Use Tinkerforge repository rather than binary repo

### 2.0.16-r3
* Update to Alpine 3.19.0
### 2.0.16-r2
* Update to Alpine 3.17.3
### 2.0.16-r1
* Update Tinkerforge MQTT translation proxy v2.16
### 2.0.15-r2
* Update Alpine 3.17.0
### 2.0.15-r1
* Update Tinkerforge MQTT translation proxy v2.0.15
* Update Alpine 3.15.0
### 2.0.13-r1
* Update Tinkerforge MQTT translation proxy v2.0.13
* Update Alpine 3.13.2
### 2.0.9-r1
* Update Tinkerforge MQTT translation proxy to v2.0.9
### 2.0.8-r1
* Update Tinkerforge MQTT translation proxy to v2.0.8
* Update Alpine to 3.11.3
### 2.0.7-r2
* Minimize image size and repo, separate download and image creation
### 2.0.7-r1
* Tinkerforge MQTT translation proxy v2.0.7
### 2.0.6-r1
* Tinkerforge MQTT translation proxy v2.0.6
